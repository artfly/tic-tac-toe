package presenter;

import model.Field;
import view.TicTacView;

public class TicTacPresenter implements Runnable {
    private Field field;
    private TicTacView view;

    public TicTacPresenter(TicTacView view) {
        this.view = view;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public void run() {
        view.attachPresenter(this);
        view.start(field.toString());
    }

    public void addPiece(int pieceNum) {
        field.update(pieceNum);
        view.update(field.toString());
        if(field.hasEnded()) {
            view.end(field.getWinner());
        }
    }

    public void newGame() {
        field.clear();
        view.start(field.toString());
    }

    public void setUserPiece(String pieceType) {
        field.setPieces(pieceType);
        view.update(field.toString());
    }
}
